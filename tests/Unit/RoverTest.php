<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Rover;
use App\Plateau;
use App\Http\Controllers\RoverController;

class RoverTest extends TestCase
{

    public function testInstructions()
    {
        $rover = new Rover();
        $plateau = new Plateau();
        $plateau->horizontalLength = 5;
        $plateau->verticalLength = 5;
        $rover->plateau = $plateau;
        $rover->horizontalCoordinate = 1;
        $rover->verticalCoordinate = 2;
        $rover->cardinalPoint = 'N';
        $rover->instructions = 'LMLMLMLMM';
        $result = $rover->move();

        $this->assertTrue($result == '1 3 N');
    }
}
