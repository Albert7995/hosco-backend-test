# Rover 
This application is made with Laravel Framework. 
The command is created using Laravel Artisan. 

## Requirements
- PHP version: `7.1`
- Laravel version: `5.7`
- Docker version: `18`

## Setup
1. Clone the project
2. Clone laradock inside the project `git clone https://github.com/Laradock/laradock.git`
3. Configure the laradock .env file according your enviroment
4. Run `composer install` in console
5. Change directory to laradock (cd laradock)
6. Run docker-compose up -d nginx to up the server

## Tests
Run the rover instructions test executing `vendor\bin\phpunit`

## Instructions to run the command
`php artisan rover:move` : This command will ask you for the rover data and will move the rovers

## Instructions to run the api 
The api endpoint is `/api/rover`
Api parameters:
plateau
rover_position[]
rover_instructions[]









