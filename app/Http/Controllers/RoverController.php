<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support\RoverManager;


class RoverController extends Controller
{
    public function start(Request $request)
    {
        $data['plateau'] = $request->plateau;

        for($i = 0; $i < count($request->rover_position); $i++){
            $data['rovers'][$i]['position'] = $request->rover_position[$i];
            $data['rovers'][$i]['instructions'] = $request->rover_instructions[$i];
        }

        $RoverManager =  new RoverManager($data);
        $rovers = $RoverManager->createRovers();
        $response = RoverManager::moveRovers($rovers);
        return nl2br($response);
    }

}
