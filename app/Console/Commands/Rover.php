<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Support\RoverManager;
use App\Http\Controllers\RoverController;
use App\Exceptions\PlateauException;

class Rover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rover:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command get the plateau and rover data and returns the rovers position';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [];
        $plateauData = $this->ask('Introduce the Plateau data');
        $data['plateau'] = $plateauData;
        $introduceMoreRovers = true;
        $rovers = [];
        $i = 0;
        while ($introduceMoreRovers) {
            $roverPositionData = $this->ask('Introduce the rover position');
            $roverInstructionsData = $this->ask('Introduce the rover instructions');
            $data['rovers'][$i]['position'] = $roverPositionData;
            $data['rovers'][$i]['instructions'] = $roverInstructionsData;

            $introduceMoreRoversResponse = $this->ask('Do you want to input another rover? (yes/no)');
            switch ($introduceMoreRoversResponse) {
            case "yes":
            $introduceMoreRovers = true;
            break;
            case "no":
            $introduceMoreRovers = false;
            break;
            }
            $i ++;
        }
        $RoverManager =  new RoverManager($data);
        $rovers = $RoverManager->createRovers();
        $this->info("Moving rovers!");
        try {
            $this->info(RoverManager::moveRovers($rovers));
        } catch (PlateauException $e) {
            $this->error("The rover cannot move in these coordinates");
        }

    }
}
