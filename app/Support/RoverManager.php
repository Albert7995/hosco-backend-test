<?php

namespace App\Support;

use App\Plateau;
use App\Rover;

class RoverManager
{
    /** @var string */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function createPlateau(){
        $plateau = new Plateau();
        $plateauArray = explode(" ", $this->data['plateau']);
        $plateau->horizontalLength = $plateauArray[0];
        $plateau->verticalLength = $plateauArray[1];
        return $plateau;
    }

    public function createRovers(){
        $rovers = [];
        foreach($this->data['rovers'] as $roverData){
            $rover = new Rover();
            $positionArray = explode(" ", $roverData['position']);

            $rover->horizontalCoordinate = $positionArray[0];
            $rover->verticalCoordinate = $positionArray[1];
            $rover->cardinalPoint = $positionArray[2];
            $rover->instructions = $roverData['instructions'];
            $rover->plateau = $this->createPlateau();
            $rovers[] = $rover;
        }
        return $rovers;
    }

    public static function moveRovers($rovers)
    {
        $response = "";
        foreach ($rovers as $rover){

            $response .=  $rover->move()."\n";
        }
        return $response;

    }







}
