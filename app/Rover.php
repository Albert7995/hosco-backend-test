<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\PlateauException;

class Rover extends Model
{
    const NORTH = 'N';
    const SOUTH = 'S';
    const EAST = 'E';
    const WEST = 'W';
    CONST RIGHT = 'R';
    CONST LEFT = 'L';
    CONST MOVE_FORWARD = 'M';

    public function move()
    {
        $instructionsArray = str_split($this->instructions);

        foreach ($instructionsArray as $instruction) {
            switch ($instruction) {
                case(self::RIGHT):
                $this->turnRight();
                break;
                case (self::LEFT):
                $this->turnLeft();
                break;
                case(self::MOVE_FORWARD):
                $this->moveForward();
                break;

            }
        }
        return $this->horizontalCoordinate." ".$this->verticalCoordinate." ".$this->cardinalPoint;
    }

    public function turnRight()
    {
        switch ($this->cardinalPoint) {
            case(self::NORTH):
            $this->cardinalPoint = self::EAST;
            break;
            case (self::EAST):
            $this->cardinalPoint = self::SOUTH;
            break;
            case(self::SOUTH):
            $this->cardinalPoint = self::WEST;
            break;
            case(self::WEST):
            $this->cardinalPoint = self::NORTH;
            break;
        }
    }

    public function turnLeft()
    {
        switch ($this->cardinalPoint) {
            case(self::NORTH):
            $this->cardinalPoint = self::WEST;
            break;
            case (self::WEST):
            $this->cardinalPoint = self::SOUTH;
            break;
            case(self::SOUTH):
            $this->cardinalPoint = self::EAST;
            break;
            case(self::EAST):
            $this->cardinalPoint = self::NORTH;
            break;
        }
    }

    public function moveForward()
    {
        switch ($this->cardinalPoint) {
            case(self::NORTH):
            $this->moveNorth();
            break;
            case (self::WEST):
            $this->moveWest();
            break;
            case(self::SOUTH):
            $this->moveSouth();
            break;
            case(self::EAST):
            $this->moveEast();
            break;
        }
    }

    private function moveNorth()
    {
        $this->verticalCoordinate++;
        if ($this->verticalCoordinate > $this->plateau->verticalLength) {
            throw new PlateauException($this);
        }
    }

    private function moveSouth()
    {
        $this->verticalCoordinate--;
        if ($this->verticalCoordinate < 0) {
            throw new PlateauException($this);
        }
    }

    private function moveEast()
    {
        $this->horizontalCoordinate++;
        if ($this->verticalCoordinate > $this->plateau->horizontalLength) {
            throw new PlateauException($this);
        }
    }

    private function moveWest()
    {
        $this->horizontalCoordinate--;
        if ($this->horizontalCoordinate < 0) {
            throw new PlateauException($this);
        }
    }
}
